package carRental;

public class GlobalVariables {
    public static final int MINIBUS_SIZE = 12;
    public static final double T1_ARRIVAL = (1.0/(60.0/43.0));
    public static final double T2_ARRIVAL = (1.0/(60.0/19.0));
    public static final double LOADING_L_BOUND = (10.0/60.0);
    public static final double LOADING_H_BOUND = (14.0/60.0);
    public static final double UNLOADING_L_BOUND = (4.0/60.0);
    public static final double UNLOADING_H_BOUND = (12.0/60.0);
    public static final double KM_PER_MINUTE = (35.0/60.0);
    public static final double T1_TO_T2 = (0.5/KM_PER_MINUTE);
    public static final double T2_TO_T3 = (2.5/KM_PER_MINUTE);
    public static final double T3_TO_T1 = (6.4/KM_PER_MINUTE);
    public static final double SERVICE_L_BOUND = 2.0;
    public static final double SERVICE_H_BOUND = 10.0;
    public static final long MIN_TO_MILLIS = 60000;
    public static final double COOLING_TIME = 60;
    public static final double WARMING_TIME = 600;
    public static final double MINIBUS_SPEED = 35.0/60.0;
}
