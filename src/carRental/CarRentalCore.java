package carRental;

import abstractCore.AbstractEventSimulation;
import carRental.Events.*;
import carRental.Model.Employee;
import carRental.Model.MiniBus;
import carRental.Model.Passenger;
import carRental.Model.Road;
import generators.DoubleGenerator;
import generators.ExponentialGenerator;
import generators.SeedGenerator;
import statistics.ConfidenceInterval;
import statistics.TimeStatistic;
import statistics.WeightedStatistic;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class CarRentalCore extends AbstractEventSimulation {
    private ExponentialGenerator arrivalT1;
    private ExponentialGenerator arrivalT2;
    private DoubleGenerator loadingTime1;
    private DoubleGenerator loadingTime2;
    private DoubleGenerator unloadingTime;
    private DoubleGenerator serviceTime;
    private LinkedList<Passenger> queueT1;
    private LinkedList<Passenger> queueT2;
    private PriorityQueue<Passenger> queueT3;
    private LinkedList<Employee> employees;
    private LinkedList<MiniBus> miniBuses;
    private TimeStatistic timeInSystem;
    private TimeStatistic totalTimeInSystem;
    private ConfidenceInterval confidenceInterval;
    private ConfidenceInterval totalConfidenceInterval;
    private long passengerInSystemSum;
    private long passengerInSystemCount;
    private int actualReplication;
    private WeightedStatistic statT1;
    private WeightedStatistic statT2;
    private WeightedStatistic statT3;
    private double lastChangeT1;
    private double LastChangeT2;
    private double lastChangeT3;
    private long seed;
    private int numberOfEmployees;
    private int numberOfBuses;
    private double minTime;
    private long sleepTime;
    private boolean isRunning;
    private final BlockingQueue<CarRentalCore> stateQueue;

    public CarRentalCore(double minTime, double maxTime, long seed, int numberOfEmployees, int numberOfBuses, long sleepTime, boolean fullSpeed, BlockingQueue<CarRentalCore> stateQueue) {
        super(maxTime, GlobalVariables.COOLING_TIME);
        this.seed = seed;
        this.numberOfEmployees = numberOfEmployees;
        this.numberOfBuses = numberOfBuses;
        this.minTime = minTime;
        this.fullSpeed = fullSpeed;
        this.stateQueue = stateQueue;
        this.sleepTime = sleepTime;
    }

    private void InitializeEmployees(int numberOfEmployees){
        for (int i = 0; i < numberOfEmployees; i++){
            employees.add(new Employee());
        }
    }

    private void InitializeBuses(int numberOfBuses){
        MiniBus hlp;
        for (int i = 0; i < numberOfBuses; i++){
            hlp = new MiniBus("Bus "+i, Road.T3toT1, currentTime);
            miniBuses.add(hlp);
            this.addEvent(new MinibusArrivalT1(this, 0.0, hlp));
        }
    }

    @Override
    public void beforeSimulation() {
        super.beforeSimulation();
        SeedGenerator.createInstance(seed);
        arrivalT1 = new ExponentialGenerator(GlobalVariables.T1_ARRIVAL, SeedGenerator.next());
        arrivalT2 = new ExponentialGenerator(GlobalVariables.T2_ARRIVAL, SeedGenerator.next());
        loadingTime1 = new DoubleGenerator(GlobalVariables.LOADING_L_BOUND, GlobalVariables.LOADING_H_BOUND, SeedGenerator.next());
        loadingTime2 = new DoubleGenerator(GlobalVariables.LOADING_L_BOUND, GlobalVariables.LOADING_H_BOUND, SeedGenerator.next());
        unloadingTime = new DoubleGenerator(GlobalVariables.UNLOADING_L_BOUND, GlobalVariables.UNLOADING_H_BOUND, SeedGenerator.next());
        serviceTime = new DoubleGenerator(GlobalVariables.SERVICE_L_BOUND, GlobalVariables.SERVICE_H_BOUND, SeedGenerator.next());
        queueT1 = new LinkedList<>();
        queueT2 = new LinkedList<>();
        queueT3 = new PriorityQueue<>();
        employees = new LinkedList<>();
        miniBuses = new LinkedList<>();
        timeInSystem = new TimeStatistic();
        confidenceInterval = new ConfidenceInterval();
        totalConfidenceInterval = new ConfidenceInterval();
        statT1 = new WeightedStatistic();
        statT2 = new WeightedStatistic();
        statT3 = new WeightedStatistic();
        totalTimeInSystem = new TimeStatistic();
        isRunning = true;
        actualReplication = 0;
    }

    @Override
    public void beforeReplication() {
        super.beforeReplication();
        currentTime = minTime;
        queueT1.clear();
        queueT2.clear();
        queueT3.clear();
        employees.clear();
        miniBuses.clear();
        statT1.reset();
        statT2.reset();
        statT3.reset();
        timeInSystem.reset();
        confidenceInterval.reset();
        lastChangeT1 = currentTime;
        LastChangeT2 = currentTime;
        lastChangeT3 = currentTime;
        passengerInSystemSum = 0;
        passengerInSystemCount = 0;
        InitializeEmployees(numberOfEmployees);
        InitializeBuses(numberOfBuses);
        InitializeArrival();
        if(!fullSpeed){
            InitializeTick();
        }
    }

    private void InitializeTick() {
        addEvent(new Tick());
    }

    @Override
    public void afterReplication() {
        totalTimeInSystem.addTime(timeInSystem.getStatistics());
        totalConfidenceInterval.addData(timeInSystem.getStatistics());
        actualReplication++;
    }

    @Override
    public void afterSimulation() {
        double[] pom = totalConfidenceInterval.get90ConfidenceInterval();
        System.out.format("%f;%f;%f %n",totalTimeInSystem.getStatistics(),pom[0], pom[1]);
        isRunning = false;
        if(fullSpeed){
            try {
                stateQueue.offer(this,1000,TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void InitializeArrival() {
        addEvent(new ArrivalT1(this, currentTime));
        addEvent(new ArrivalT2(this, currentTime));
        addEvent(new StatisticsReset(this, GlobalVariables.WARMING_TIME));
    }

    public double getArrivalT1() {
        return arrivalT1.next();
    }

    public double getArrivalT2() {
        return arrivalT2.next();
    }

    public void addPassengerQueue1(Passenger passenger) {
        statT1.addData(currentTime - lastChangeT1, queueT1.size());
        lastChangeT1 = currentTime;
        queueT1.add(passenger);
    }

    public void addPassengerQueue2(Passenger passenger) {
        statT2.addData(currentTime - LastChangeT2, queueT2.size());
        LastChangeT2 = currentTime;
        queueT2.add(passenger);
    }

    public void income(){
        passengerInSystemSum++;
        passengerInSystemCount++;
    }

    public void outcome(){
        passengerInSystemCount--;
    }

    public void unload(Passenger passenger) {
        statT3.addData(currentTime - lastChangeT3, queueT3.size());
        lastChangeT3 = currentTime;
        queueT3.add(passenger);
    }

    public Passenger pollPassenger1() {
        statT1.addData(currentTime - lastChangeT1, queueT1.size());
        lastChangeT1 = currentTime;
        return queueT1.poll();
    }

    public Passenger pollPassenger2() {
        statT2.addData(currentTime - LastChangeT2, queueT2.size());
        LastChangeT2 = currentTime;
        return queueT2.poll();
    }

    public Passenger pollPassenger3(){
        statT3.addData(currentTime - lastChangeT3, queueT3.size());
        lastChangeT3 = currentTime;
        return queueT3.poll();
    }

    public void refresh() {
        if(!fullSpeed){
            try {
                stateQueue.offer(this,1000,TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isWaitingT1(){
        return !queueT1.isEmpty();
    }

    public boolean isWaitingT2(){
        return !queueT2.isEmpty();
    }

    public double getLoadingTime1() {
        return loadingTime1.next();
    }

    public double getLoadingTime2() {
        return loadingTime2.next();
    }

    public double getUnloadingTime() {
        return unloadingTime.next();
    }

    public boolean isFreeEmployee(){
        return !employees.isEmpty();
    }

    public Employee pollEmployee(){
        return employees.poll();
    }

    public void addEmployee(Employee employee){
        employees.add(employee);
    }

    public double getServiceTime(){
        return serviceTime.next();
    }

    public boolean isWaitingForService() {
        return !queueT3.isEmpty();
    }

    public void addStatistic(double timeToAdd) {
            timeInSystem.addTime(timeToAdd);
            confidenceInterval.addData(timeToAdd);
    }

    public double getTimeInSystem(){
        return timeInSystem.getStatistics();
    }

    public double getTotalTimeInSystem(){
        return totalTimeInSystem.getStatistics();
    }

    public double[] getConfidenceInterval(){
        return confidenceInterval.get90ConfidenceInterval();
    }

    public double[] getTotalConfidenceInterval(){
        return totalConfidenceInterval.get90ConfidenceInterval();
    }

    public long getSleepTime() {
        return sleepTime;
    }

    public int getActualReplication(){ return actualReplication;}

    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public double getMaxTime(){
        return maxTime;
    }

    public void resetStatistics(){
        timeInSystem.reset();
        confidenceInterval.reset();
    }

    public LinkedList<MiniBus> getMiniBuses() {
        return miniBuses;
    }

    public long getPassengerInSystemCount() {
        return passengerInSystemCount;
    }

    public long getPassengerInSystemSum() {
        return passengerInSystemSum;
    }

    public String[] getQueueLengths() {
        String[] res = new String[4];
        res[0] = queueT1.size()+"";
        res[1] = queueT2.size()+"";
        res[2] = queueT3.size()+"";
        res[3] = employees.size()+"";
        return res;
    }
}
