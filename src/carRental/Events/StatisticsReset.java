package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;

public class StatisticsReset extends CarRentalEvent {
    public StatisticsReset(AbstractEventSimulation simulation, double time) {
        super(simulation, time);
        this.name = "StatisticsReset";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        typedCore.resetStatistics();
    }
}
