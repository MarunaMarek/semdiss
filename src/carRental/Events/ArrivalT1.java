package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;
import carRental.Model.Passenger;

public class ArrivalT1 extends CarRentalEvent {
    private Passenger passenger;

    public ArrivalT1(AbstractEventSimulation simulation, double time) {
        super(simulation, time);
        this.passenger = new Passenger(time);
        this.name = "ArrivalT1";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        typedCore.addPassengerQueue1(passenger);
        typedCore.income();
        if(typedCore.getCurrentTime()< typedCore.getMaxTime()){
            typedCore.addEvent(new ArrivalT1(simulation, time+typedCore.getArrivalT1()));
        }
        typedCore.refresh();
    }
}
