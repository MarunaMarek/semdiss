package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;
import carRental.Model.MiniBus;
import carRental.Model.Road;

public class MinibusArrivalT2 extends CarRentalEvent {

    private MiniBus miniBus;

    public MinibusArrivalT2(AbstractEventSimulation simulation, double time, MiniBus miniBus) {
        super(simulation, time);
        this.miniBus = miniBus;
        this.name = "MinibusArrivalT2";
    }

    @Override
    public void execute() {
        miniBus.setAtStop(true);
        miniBus.setNextRoad(Road.T2, time);
        CarRentalCore typedCore = (CarRentalCore) simulation;
        if(!typedCore.isWaitingT2() || !miniBus.isFree()){
            typedCore.addEvent(new MinibusLeaveT2(simulation, time, miniBus));
        }
        else {
            miniBus.addPassenger(typedCore.pollPassenger2());
            typedCore.addEvent(new MinibusArrivalT2(simulation, time+typedCore.getLoadingTime2(), miniBus));
        }
        typedCore.refresh();
    }
}
