package carRental.Events;

import abstractCore.AbstractEventSimulation;
import abstractCore.Event;
import carRental.CarRentalCore;
import carRental.GlobalVariables;
import carRental.Model.MiniBus;
import carRental.Model.Road;

public class MinibusLeaveT3 extends CarRentalEvent {
    private MiniBus miniBus;

    public MinibusLeaveT3(AbstractEventSimulation simulation, double time, MiniBus miniBus) {
        super(simulation, time);
        this.miniBus = miniBus;
        this.name = "MinibusLeaveT3";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        miniBus.setAtStop(false);
        miniBus.setNextRoad(Road.T3toT1, time);
        typedCore.addEvent(new MinibusArrivalT1(simulation, time + GlobalVariables.T3_TO_T1, miniBus));
        typedCore.refresh();
    }
}
