package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;
import carRental.GlobalVariables;
import carRental.Model.MiniBus;
import carRental.Model.Passenger;
import carRental.Model.Road;

public class MinibusArrivalT3 extends CarRentalEvent {
    private MiniBus miniBus;

    public MinibusArrivalT3(AbstractEventSimulation simulation, double time, MiniBus miniBus) {
        super(simulation, time);
        this.miniBus = miniBus;
        this.name = "MinibusArrivalT3";
    }

    @Override
    public void execute() {
        miniBus.setAtStop(true);
        miniBus.setNextRoad(Road.T3, time);
        CarRentalCore typedCore = (CarRentalCore) simulation;
        if(miniBus.getFreeSpace() == GlobalVariables.MINIBUS_SIZE){
            typedCore.addEvent(new MinibusLeaveT3(simulation, time, miniBus));
        }
        else {
            Passenger pas = miniBus.pollPassenger();
            double unload = typedCore.getUnloadingTime();
            pas.setStartOfWaiting(time+unload);
            if(typedCore.isFreeEmployee()){
                pas.setStartOfWaiting(time+unload);
                typedCore.addEvent(new StartOfService(simulation, time + unload, pas, typedCore.pollEmployee()));
            }else {
                typedCore.unload(pas);
            }
            typedCore.addEvent(new MinibusArrivalT3(simulation, time + typedCore.getUnloadingTime(), miniBus));
        }
        typedCore.refresh();
    }
}
