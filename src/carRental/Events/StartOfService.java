package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;
import carRental.Model.Employee;
import carRental.Model.Passenger;

public class StartOfService extends CarRentalEvent {
    private Passenger passenger;
    private Employee employee;

    public StartOfService(AbstractEventSimulation simulation, double time, Passenger passenger, Employee employee) {
        super(simulation, time);
        this.passenger = passenger;
        this.employee = employee;
        this.name = "StartOfService";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        typedCore.addEvent(new EndOfService(simulation, time + typedCore.getServiceTime(), passenger, employee));
        typedCore.refresh();
    }
}
