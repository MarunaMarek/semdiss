package carRental.Events;

import abstractCore.AbstractEventSimulation;
import abstractCore.Event;
import carRental.CarRentalCore;
import carRental.GlobalVariables;
import carRental.Model.MiniBus;
import carRental.Model.Road;

public class MinibusLeaveT2 extends CarRentalEvent {
    private MiniBus miniBus;
    public MinibusLeaveT2(AbstractEventSimulation simulation, double time, MiniBus miniBus) {
        super(simulation, time);
        this.miniBus = miniBus;
        this.name = "MinibusLeaveT2";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        miniBus.setAtStop(false);
        miniBus.setNextRoad(Road.T2toT3, time);
        typedCore.addEvent(new MinibusArrivalT3(simulation, time+GlobalVariables.T2_TO_T3, miniBus));
        typedCore.refresh();
    }
}
