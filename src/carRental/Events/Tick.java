package carRental.Events;

import abstractCore.Event;
import carRental.CarRentalCore;

public class Tick extends Event {

    public Tick(){
        this.name = "Tick";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        typedCore.refresh();
        try {
            Thread.sleep(1000/typedCore.getSleepTime());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.time += (1000.0/60000.0);
        typedCore.addEvent(this);
    }
}
