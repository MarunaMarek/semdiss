package carRental.Events;

import abstractCore.AbstractEventSimulation;
import abstractCore.Event;

public abstract class CarRentalEvent extends Event {

    public CarRentalEvent(AbstractEventSimulation simulation, double time){
        this.simulation = simulation;
        this.time = time;
    }
}
