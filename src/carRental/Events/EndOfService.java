package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;
import carRental.Model.Employee;
import carRental.Model.Passenger;

public class EndOfService extends CarRentalEvent {
    private Passenger passenger;
    private Employee employee;

    public EndOfService(AbstractEventSimulation simulation, double time, Passenger passenger, Employee employee) {
        super(simulation, time);
        this.passenger = passenger;
        this.employee = employee;
        this.name = "EndOfService";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        typedCore.addStatistic(time - passenger.getArrivalTime());
        if(typedCore.isWaitingForService()){
            typedCore.addEvent(new StartOfService(simulation, time, typedCore.pollPassenger3(), employee));
        }else {
            typedCore.addEmployee(employee);
        }
        typedCore.outcome();
        typedCore.refresh();
    }
}
