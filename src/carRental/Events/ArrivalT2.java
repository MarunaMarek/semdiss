package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;
import carRental.Model.Passenger;

public class ArrivalT2 extends CarRentalEvent {
    private Passenger passenger;

    public ArrivalT2(AbstractEventSimulation simulation, double time) {
        super(simulation, time);
        this.passenger = new Passenger(time);
        this.name = "ArrivalT2";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        typedCore.addPassengerQueue2(passenger);
        typedCore.income();
        if(typedCore.getCurrentTime()<typedCore.getMaxTime()){
            typedCore.addEvent(new ArrivalT2(simulation, time + typedCore.getArrivalT2()));
        }
        typedCore.refresh();
    }
}
