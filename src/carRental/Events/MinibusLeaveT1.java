package carRental.Events;

import abstractCore.AbstractEventSimulation;
import abstractCore.Event;
import carRental.CarRentalCore;
import carRental.GlobalVariables;
import carRental.Model.MiniBus;
import carRental.Model.Road;

public class MinibusLeaveT1 extends CarRentalEvent {
    private MiniBus miniBus;

    public MinibusLeaveT1(AbstractEventSimulation simulation, double time, MiniBus miniBus) {
        super(simulation, time);
        this.miniBus = miniBus;
        this.name = "MinibusLeaveT1";
    }

    @Override
    public void execute() {
        CarRentalCore typedCore = (CarRentalCore) simulation;
        miniBus.setAtStop(false);
        miniBus.setNextRoad(Road.T1toT2, time);
        typedCore.addEvent(new MinibusArrivalT2(simulation, time+GlobalVariables.T1_TO_T2, miniBus));
        typedCore.refresh();
    }
}
