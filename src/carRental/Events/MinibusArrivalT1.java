package carRental.Events;

import abstractCore.AbstractEventSimulation;
import carRental.CarRentalCore;
import carRental.Model.MiniBus;
import carRental.Model.Road;

public class MinibusArrivalT1 extends CarRentalEvent {

    private MiniBus miniBus;

    public MinibusArrivalT1(AbstractEventSimulation simulation, double time, MiniBus miniBus) {
        super(simulation, time);
        this.miniBus = miniBus;
        this.name = "MinibusArrivalT1";
    }

    @Override
    public void execute() {
        miniBus.setAtStop(true);
        miniBus.setNextRoad(Road.T1, time);
        CarRentalCore typedCore = (CarRentalCore) simulation;
        if(!typedCore.isWaitingT1() || !miniBus.isFree()){
            typedCore.addEvent(new MinibusLeaveT1(simulation, time, miniBus));
        }
        else {
            miniBus.addPassenger(typedCore.pollPassenger1());
            typedCore.addEvent(new MinibusArrivalT1(simulation, time+typedCore.getLoadingTime1(), miniBus));
        }
        typedCore.refresh();
    }
}
