package carRental.Model;

public enum Road {
    T1toT2(1),
    T2toT3(2),
    T3toT1(3),
    T1(4),
    T2(5),
    T3(6)
    ;

    private int road;

    Road(int road){
        this.road = road;
    }

    public int getRoad() {
        return road;
    }
}
