package carRental.Model;

public class Employee {
    private boolean isFree;

    public Employee(){
        isFree = true;
    }


    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
