package carRental.Model;

public class Passenger implements Comparable<Passenger>{
    private double arrivalTime;
    private double startOfWaiting;

    public Passenger(){}

    public Passenger(double arrivalTime){
        this.arrivalTime = arrivalTime;
    }

    public double getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(double arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public double getStartOfWaiting() {
        return startOfWaiting;
    }

    public void setStartOfWaiting(double startOfWaiting) {
        this.startOfWaiting = startOfWaiting;
    }

    @Override
    public int compareTo(Passenger passenger) {
        return Double.compare(startOfWaiting, passenger.startOfWaiting);
    }


}
