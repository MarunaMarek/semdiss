package carRental.Model;

import carRental.GlobalVariables;
import java.util.LinkedList;

public class MiniBus {
    private LinkedList<Passenger> passengers;
    private String name;
    private Road road;
    private double lastChange;
    private boolean atStop;

    public MiniBus(String name, Road actualRoad, double lastChange){
        this.name = name;
        this.passengers = new LinkedList<>();
        this.road = actualRoad;
        this.lastChange = lastChange;
    }

    public void addPassenger(Passenger passenger){
        passengers.add(passenger);
    }

    public Passenger pollPassenger(){
        return passengers.poll();
    }


    public int getFreeSpace(){
        return GlobalVariables.MINIBUS_SIZE-passengers.size();
    }

    public boolean isFree(){
        return getFreeSpace()>0;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNextRoad(Road nextRoad, double lastChange){
        road = nextRoad;
        this.lastChange = lastChange;
    }

    public String getActualData(double time){
        double distance = 0.0;
        if(!isAtStop()){
            double hlpTime = time - lastChange;
            distance = GlobalVariables.MINIBUS_SPEED*hlpTime;
        }
        return String.format("%10s  %3d %15f    %10s",name, getFreeSpace(), distance, road);
    }

    @Override
    public String toString() {
        return String.format("%s has freespace %d and is on the road %s",name, getFreeSpace(), road);
    }

    public boolean isAtStop() {
        return atStop;
    }

    public void setAtStop(boolean atStop) {
        this.atStop = atStop;
    }
}