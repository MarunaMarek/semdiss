package exceptions;

public class TimeException extends Exception{

    public TimeException(String msg){
        super(msg);
    }
    public TimeException(Throwable cause){
        super(cause);
    }
    public TimeException(String msg, Throwable cause){
        super(msg, cause);
    }
}
