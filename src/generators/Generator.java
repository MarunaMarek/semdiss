package generators;

public interface Generator<T> {
    public T next();
}
