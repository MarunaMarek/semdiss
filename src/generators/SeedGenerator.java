package generators;

import java.util.Random;

public class SeedGenerator {

    private static Random seedGenerator;

    private SeedGenerator(long seed){

    }

    public static void createInstance(long seed) {
        if(seedGenerator==null) {
            if(seed != 0) {
                seedGenerator = new Random(seed);
            }
            else seedGenerator = new Random();
        }
    }

    public static long next(){
        return seedGenerator.nextLong();
    }

}
