package generators;

public class ExponentialGenerator implements Generator <Double> {
    private DoubleGenerator doubleGenerator;
    private double lambda;

    public ExponentialGenerator(double lambda){
        doubleGenerator = new DoubleGenerator();
        this.lambda = lambda;
    }

    public ExponentialGenerator(double lambda, long seed){
        doubleGenerator = new DoubleGenerator(seed);
        this.lambda = lambda;
    }

    @Override
    public Double next() {
        return (-1.0*Math.log(doubleGenerator.next())/lambda);
    }
}
