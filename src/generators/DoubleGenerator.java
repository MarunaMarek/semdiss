package generators;

import java.util.Random;

public class DoubleGenerator implements Generator<Double> {
    private Random generator;
    private double from;
    private double to;

    public DoubleGenerator(){
        generator = new Random();
        from = 0;
        to = 1;
    }

    public DoubleGenerator(long seed){
        generator = new Random(seed);
        from = 0;
        to = 1;
    }

    public DoubleGenerator(double from, double to){
        generator = new Random();
        this.from = from;
        this.to = to;
    }

    public DoubleGenerator(double from, double to, long seed){
        generator = new Random(seed);
        this.from = from;
        this.to = to;
    }

    @Override
    public Double next() {
        return (generator.nextDouble()*(to-from))+from;
    }
}
