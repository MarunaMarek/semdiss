package generators;

import java.util.Random;

public class IntegerGenerator implements Generator<Integer> {

    private Random generator;
    private int minBound;
    private int maxBound;

    public IntegerGenerator(long seed){
        generator = new Random(seed);
    }

    public IntegerGenerator(long seed, int min, int max){
        generator = new Random(seed);
        minBound = min;
        maxBound = max;
    }

    /**
     * Generate a pseudorandom, uniformly distributed int value
     * @return int value between @minBound inclusive and @maxBound exclusive
     */
    @Override
    public Integer next() {
        return generator.nextInt(maxBound - minBound) + minBound;
    }
}
