package statistics;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ConfidenceInterval {
    private double sum;
    private int count;
    private ConcurrentLinkedQueue<Double> numbers;

    public ConfidenceInterval(){
        reset();
    }

    public void reset() {
        sum = 0.0;
        count = 0;
        numbers = new ConcurrentLinkedQueue<>();
    }

    public void addData(double number){
        sum+=number;
        numbers.add(number);
        count++;
    }

    public double[] get95ConfidenceInterval(){
        return getConfidenceInterval(1.96);
    }

    private double getStandardDeviation(double mean) {
        double squaredDifference = 0.0;
        for (double number :
                numbers) {
            squaredDifference += ((number - mean)*(number - mean));
        }
        return Math.sqrt(squaredDifference/(double)count);
    }

    private double[] getConfidenceInterval(double percentage){
        double mean = sum / (double)count;
        double standardDeviation = getStandardDeviation(mean);
        double temp = percentage * standardDeviation / Math.sqrt(count);
        return new double[]{mean - temp, mean + temp};
    }

    public double[] get90ConfidenceInterval(){
        return getConfidenceInterval(1.645);
    }
}
