package statistics;

public class TimeStatistic {
    private int totalCount;
    private double totalTime;

    public TimeStatistic(){
        reset();
    }

    public void reset() {
        totalCount = 0;
        totalTime = 0.0;
    }

    public void addTime(double time){
        totalCount++;
        totalTime+=time;
    }

    public double getStatistics(){
        return (totalTime / (double) totalCount);
    }

}
