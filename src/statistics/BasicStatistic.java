package statistics;

public class BasicStatistic {
    private int totalCount;
    private int successfulCount;

    public BasicStatistic(){
        reset();
    }

    public void reset(){
        totalCount = 0;
        successfulCount = 0;
    }

    public void success(){
        totalCount ++;
        successfulCount ++;
    }

    public void failure(){
        totalCount ++;
    }

    public double getStatistic(){
        return ((double) successfulCount/ (double) totalCount);
    }
}
