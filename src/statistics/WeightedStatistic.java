package statistics;

public class WeightedStatistic {

    private double weightCount;
    private double totalCount;

    public WeightedStatistic() {
        reset();
    }

    public void reset() {
        weightCount = 0.0;
        totalCount = 0.0;
    }

    public double getStatistics(){
        return (totalCount / weightCount);
    }

    public void addData(double weight, double value){
        weightCount+= weight;
        totalCount += weight*value;
    }
}
