package abstractCore;

public interface ResultListener {
    void publishResult();
}
