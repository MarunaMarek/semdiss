package abstractCore;

public abstract class Event implements Comparable<Event>{
    protected String name;
    protected double time;
    protected AbstractEventSimulation simulation;

    public abstract void execute();

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public int compareTo(Event event){
        return Double.compare(time, event.time);
    }

    @Override
    public String toString() {
        return String.format("%30s:%30f",name,time);
    }
}
