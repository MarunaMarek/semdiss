package abstractCore;

import exceptions.TimeException;

import java.util.PriorityQueue;

public abstract class AbstractEventSimulation extends AbstractMonteCarlo {
    protected double currentTime;
    private PriorityQueue<Event> timeline;
    protected double maxTime;
    protected boolean fullSpeed;
    protected double coolingTime;

    public AbstractEventSimulation(double maxTime, double coolingTime){
        this.maxTime = maxTime;
        timeline = new PriorityQueue<>();
        currentTime = 0.0;
        this.coolingTime = coolingTime;
    }

    public void addEvent(Event event) {
        if(event.getTime() >= currentTime) {
            event.simulation = this;
            timeline.add(event);
        }
        else System.out.print("Tried to add event with lower time than current simulation time!");

    }

    @Override
    public void replication() throws TimeException {
        while (currentTime < (maxTime + coolingTime) && !timeline.isEmpty()){
            if(pause) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }
            if(stop) break;
            Event hlpEvent = timeline.poll();
            if(hlpEvent.getTime() >= currentTime){
                currentTime = hlpEvent.getTime();
                hlpEvent.execute();
            }
            else throw new TimeException("Trying to execute event from past!");
        }
    }

    public PriorityQueue<Event> getTimeline(){
        return new PriorityQueue<Event>(timeline);
    }

    @Override
    public void beforeReplication() {
        timeline.clear();
    }

    public void stopSimulation() {
        stop = true;
    }

    public void pauseSimulation() {
        pause = true;
    }

    public void resumeSimulation() {
        pause = false;
    }

    public double getCurrentTime() { return currentTime;}
}
