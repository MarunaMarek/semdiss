package abstractCore;

import exceptions.TimeException;

public abstract class AbstractMonteCarlo {
    protected boolean stop;
    protected boolean pause;
    protected ResultListener resultListener;
    protected int numberOfReplications;
    protected int actualReplication;

    public void simulate(int numberOfReplications){
        beforeSimulation();
        this.numberOfReplications = numberOfReplications;
        for (actualReplication = 0; actualReplication<numberOfReplications; actualReplication++){
            beforeReplication();
            try {
                replication();
            } catch (TimeException e) {
                e.printStackTrace();
            }
            afterReplication();
            if(stop) break;
        }
        afterSimulation();
    }

    public void beforeSimulation(){
        stop = false;
        pause = false;
    }
    public abstract void afterSimulation();
    public abstract void beforeReplication();
    public abstract void afterReplication();
    public abstract void replication() throws TimeException;

    public void addListener(ResultListener resultListener){
        this.resultListener = resultListener;
    }
}
