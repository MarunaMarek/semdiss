package gui;

import carRental.CarRentalCore;
import carRental.Model.MiniBus;
import javafx.animation.AnimationTimer;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class AirRental implements Initializable {
    @FXML
    public Slider replicationNumberSlider;
    @FXML
    public Slider employeesNumberSlider;
    @FXML
    public Slider minibusNumberSlider;
    @FXML
    public Slider simulationSpeedSlider;
    @FXML
    public Button runStopButton;
    @FXML
    public Button pauseResumeButton;
    @FXML
    public CheckBox fullSpeedCheckBox;
    @FXML
    public Button experiment1Button;
    @FXML
    public Button experiment2Button;
    @FXML
    public Label actualConfidenceLabel;
    @FXML
    public Label totalConfidenceLabel;
    @FXML
    public Label actualTimeInSystemLabel;
    @FXML
    public Label totalTimeInSystemLabel;
    @FXML
    public Label actualReplicationLabel;
    @FXML
    public Label actualTimeLabel;
    @FXML
    public ListView<String> minibusListView;
    @FXML
    public Label actualCustomerCount;
    @FXML
    public Label totalCustomerCount;
    @FXML
    public Label countT1;
    @FXML
    public Label countT2;
    @FXML
    public Label countT3;
    @FXML
    public Label employeesCount;

    private boolean running;
    private boolean paused;

    private CarRentalCore carRentalCore;
    private final BlockingQueue<CarRentalCore> stateQueue = new ArrayBlockingQueue<>(1);
    private final LongProperty lastUpdate = new SimpleLongProperty();
    private final long minUpdateInterval = 0;
    private Stage stage;
    private BarChart<String, Number> bc;


    private void publishResult(CarRentalCore carRentalCore) {
        if (!fullSpeedCheckBox.isSelected()) {
            actualTimeInSystemLabel.setText(String.format("%f", carRentalCore.getTimeInSystem()));
            double[] actual = carRentalCore.getConfidenceInterval();
            actualConfidenceLabel.setText(String.format("< %f ; %f >", actual[0], actual[1]));
            actualReplicationLabel.setText(carRentalCore.getActualReplication() + "");
            actualTimeLabel.setText(TimeConverter.convertToDate(carRentalCore.getCurrentTime()));
            minibusListView.getItems().clear();
            actualCustomerCount.setText(carRentalCore.getPassengerInSystemCount()+"");
            totalCustomerCount.setText(carRentalCore.getPassengerInSystemSum()+"");
            String[] pom = carRentalCore.getQueueLengths();
            countT1.setText(pom[0]);
            countT2.setText(pom[1]);
            countT3.setText(pom[2]);
            employeesCount.setText(pom[3]);
            for (MiniBus m : carRentalCore.getMiniBuses()) {
                minibusListView.getItems().add(m.getActualData(carRentalCore.getCurrentTime()));
            }
            if (!carRentalCore.isRunning()) {
                runStopButton.setText("Run");
                running = false;
            }
        }
        double[] total = carRentalCore.getTotalConfidenceInterval();
        totalTimeInSystemLabel.setText(String.format("%f", carRentalCore.getTotalTimeInSystem()));
        totalConfidenceLabel.setText(String.format("< %f ; %f >", total[0], total[1]));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        running = false;
        paused = false;
        pauseResumeButton.setDisable(true);
        simulationSpeedSlider.valueProperty().addListener(((observable, oldValue, newValue) -> speedChanged(newValue.intValue())));
        AnimationTimer animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (now - lastUpdate.get() > minUpdateInterval) {
                    CarRentalCore hlpCore;
                    hlpCore = stateQueue.poll();
                    if (hlpCore != null) {
                        publishResult(hlpCore);
                    }
                    lastUpdate.set(now);
                }
            }
        };
        animationTimer.start();
    }

    public void runButtonPressed() {
        if (!running) {
            running = true;
            pauseResumeButton.setDisable(false);
            carRentalCore = new CarRentalCore(0, 43200, 0, (int) employeesNumberSlider.getValue(),
                    (int) minibusNumberSlider.getValue(), (long) simulationSpeedSlider.getValue(),
                    fullSpeedCheckBox.isSelected(), stateQueue);

            Thread thread = new Thread(() -> carRentalCore.simulate((int) replicationNumberSlider.getValue()));
            thread.setDaemon(true);
            thread.start();
            runStopButton.setText("Stop");
        } else {
            running = false;
            carRentalCore.stopSimulation();
            pauseResumeButton.setDisable(true);
            runStopButton.setText("Run");
        }
    }

    public void pauseButtonPressed() {
        if (!paused) {
            paused = true;
            carRentalCore.pauseSimulation();
            pauseResumeButton.setText("Resume");
        } else {
            paused = false;
            carRentalCore.resumeSimulation();
            pauseResumeButton.setText("Pause");
        }
    }

    private XYChart.Series setUpBarChart(String countOf) {
        stage = new Stage();
        stage.setTitle("Bar Chart");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        bc = new BarChart<>(xAxis, yAxis);
        bc.setTitle("Dependency time/"+countOf);
        xAxis.setLabel(countOf);
        yAxis.setLabel("Time");
        yAxis.setForceZeroInRange(false);
        return new XYChart.Series();
    }

    private void showGraph(XYChart.Series series1) {
        Scene scene = new Scene(bc, 800, 600);
        bc.getData().addAll(series1);
        stage.setScene(scene);
        stage.show();
    }

    public void experiment1ButtonPressed() {
        fullSpeedCheckBox.setSelected(true);
        XYChart.Series series = setUpBarChart("Count of minibuses");
        for (int i = 5; i <= 20; i++) {
            stateQueue.clear();
            carRentalCore = new CarRentalCore(0, 43200, 0, 19, i, 1, true, this.stateQueue);
            carRentalCore.simulate(10);
            series.getData().add(new XYChart.Data<>(i + "", carRentalCore.getTotalTimeInSystem()));
        }
        showGraph(series);
    }

    public void experiment2ButtonPressed() {
        fullSpeedCheckBox.setSelected(true);
        XYChart.Series series = setUpBarChart("Count of employees");
        for (int i = 7; i <= 20; i++) {
            stateQueue.clear();
            carRentalCore = new CarRentalCore(0, 43200, 0, i, 16, 1, true, this.stateQueue);
            carRentalCore.simulate(10);
            series.getData().add(new XYChart.Data<>(i + "", carRentalCore.getTotalTimeInSystem()));
        }
        showGraph(series);
    }

    private void speedChanged(int newValue) {
        if (carRentalCore != null) {
            carRentalCore.setSleepTime((long) newValue);
        }
    }
}
