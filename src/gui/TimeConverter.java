package gui;

import carRental.GlobalVariables;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeConverter {
    private static Date date = new Date();
    public static String convertToDate(double time){
        date.setTime((long)(time*GlobalVariables.MIN_TO_MILLIS));
        date.setHours(date.getHours()-1);
        String res = new SimpleDateFormat("dd HH:mm:ss").format(date);
        return res;
    }
}
