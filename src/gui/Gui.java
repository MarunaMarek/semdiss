package gui;

import carRental.CarRentalCore;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Gui extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("airRental.fxml"));
        primaryStage.setTitle("AirCar");
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setMaximized(true);
        //CarRentalCore crc = new CarRentalCore(0,42800,0,0,18,7, false);
        //crc.simulate(1000);
    }
}
